import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground, SafeAreaView, TextInput, Pressable} from 'react-native';


export default function Login({navigation}){
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isError, setIsError] = useState(false);

    const submit = () => {
        if(password == '' || username == ''){
            setIsError(true)
            if(isError == true){
                return(
                navigation.navigate("Login",{
                    screen : 'Login',
                })
                )
            }
        } else{
            navigation.navigate("Lacak COVID",{
                screen : 'Profile', params:{
                  key : username,
                }
              })
          
        }
    
      };
    return(
            
        <View style={styles.container}>
            <ImageBackground source={require('../assets/Background_login.png')} resizeMode="cover" style={styles.backgroundImage}>
                <View style={styles.contentContainer} /> 
                <View style={styles.content}>
                    <View style={styles.loginContent}>
                        <Text style={styles.login}>Login</Text>
                        <SafeAreaView>
                            <TextInput
                                style={styles.input}
                                placeholder="Username"
                                value={username}
                                onChangeText={(value)=>setUsername(value)}
                            />
                            <TextInput
                                style={styles.input}
                                placeholder="Password"
                                value={password}
                                onChangeText={(value)=>setPassword(value)}
                            />
                        </SafeAreaView>
                        
                        <Text style={styles.daftar}>Belum punya akun?</Text>
                        <Pressable style={styles.button} onPress={submit}>
                            <Text style={styles.loginButton}>Login</Text>
                        </Pressable>
                        
                    </View>
                    
                </View>
            </ImageBackground>
        </View>
    )
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    header: {
        height : 50,
        marginTop:100,
        justifyContent:'center',
        alignItems: 'center'
    },

    backgroundImage: {
        flex: 1,
        justifyContent: "flex-start"
    },

    contentContainer: {
        flex: 1 // pushes the footer to the end of the screen
    },

    content:{
        height: 400,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50
    },

    loginContent:{
        marginVertical: 50,
        marginHorizontal: 50
    },

    login:{
        color: '#FF725E',
        fontSize: 24,
        fontWeight: 'bold'
    },

    input: {
        height: 44,
        width: '100%',
        borderWidth: 1,
        padding: 10,
        marginTop: 32,
        borderColor: '#C3C3C3',
        backgroundColor: 'white',
        borderRadius: 8
    },

    daftar: {
        color: '#FF725E',
        fontSize: 16,
        marginTop: 13
    },

    button:{
        backgroundColor: '#FF725E',
        borderRadius: 8,
        elevation: 3,
        width: '100%',
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 29
    },

    loginButton:{
        color: 'white',
        
    }

})
import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView, SafeAreaView} from 'react-native';

export default function Profile({route}){
    return(
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.backgroundColor}>
                        <Text style={styles.profileText}>Profile</Text>
                        <View style={styles.containerProfile}>
                            <View style={styles.contentProfile}>
                                <Image 
                                    style={styles.profilePic}
                                    source={require("../assets/healthicons_ui-user-profile.png")}
                                />
                                <View>
                                    <Text style={styles.username}>
                                        {route.params.key}
                                    </Text>
                                    <Text style={styles.email}>
                                        {route.params.key}@gmail.com
                                    </Text>
                                </View>

                            </View>
                        </View>
                    </View>
                </View>
                
                <SafeAreaView>
                    
                    <View style={styles.content}>
                        <Text style={styles.contentTitle}>
                                Patuhi Protokol Kesehatan Dengan 3M
                        </Text>
                        <View style={styles.masker}>
                            <Image 
                                style={styles.maskerImage}
                                source={require("../assets/masker.png")}
                            />
                            <Text  style={styles.maskerText}>
                                Memakai Masker
                            </Text>
                        </View>
                        <View style={styles.cuci}>
                            <Text  style={styles.cuciText}>
                                Mencuci Tangan
                            </Text>
                            <Image 
                                style={styles.cuciImage}
                                source={require("../assets/cuci.png")}
                            />
                        </View>
                        <View style={styles.jarak}>
                            <Image 
                                style={styles.jarakImage}
                                source={require("../assets/jarak.png")}
                            />
                            <Text  style={styles.jarakText}>
                                Menjaga Jarak
                            </Text>
                        </View>
                    </View>
                    
                </SafeAreaView>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    backgroundColor: {
        width: '100%',
        height: 202,
        backgroundColor: '#FF725E',
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20
    },

    profileText:{
        color: 'white',
        marginHorizontal: 30,
        marginVertical: 20,
        fontSize: 32,
        fontWeight: 'bold'
    },

    containerProfile:{
        marginTop: 40,
        marginHorizontal: 30,
        borderRadius: 20,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },

    contentProfile:{
        flexDirection: 'row',

    },

    profilePic:{
        marginVertical: 20,
        marginHorizontal: 15
    },

    username:{
        marginTop: 30,
        fontWeight: 'bold',
        fontSize: 20
    },

        
    email:{
        marginTop: '10%',
        color: '#797979'
    },

    content:{
        marginTop: 100
    },
    contentTitle:{
        marginHorizontal: 30,
        fontSize: 20,
        width: '70%',
        fontWeight: 'bold'
    },

    masker:{
        backgroundColor: '#FF725E',
        flexDirection: 'row',
        paddingVertical: 20,
        marginTop: 20
    },

    maskerImage:{
        width: 170,
        height: 127,
        marginLeft: 20,
    },

    maskerText:{
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 30,
        width: '50%',
        marginLeft: 30
    },

    cuci:{
        flexDirection: 'row',
        paddingVertical: 20,
        marginTop: 20,
        marginLeft: 40
    },

    cuciText:{
        alignSelf: 'center',
        color: '#FF725E',
        fontWeight: 'bold',
        fontSize: 30,
        width: '50%',
    },

    cuciImage:{
        width: 170,
        height: 127,
        marginRight: 20
    },

    jarak:{
        backgroundColor: '#FF725E',
        flexDirection: 'row',
        paddingVertical: 20,
        marginTop: 20
    },

    jarakImage:{
        width: 170,
        height: 127,
        marginLeft: 20
    },

    jarakText:{
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 30,
        width: '50%',
        marginLeft: 30
    },
})
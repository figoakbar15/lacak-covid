import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground, SafeAreaView, TextInput} from 'react-native';



export default function Detail({route, navigation} ){
    
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <ImageBackground source={require('../assets/detail_header.png')} resizeMode="cover" style={styles.backgroundImage}>
                    <Text style={styles.titleProvinsi}> 
                        Provinsi
                    </Text>
                    <Text style={styles.Provinsi}> 
                        {route.params.key.attributes.Provinsi}
                    </Text>
                </ImageBackground>
            </View>
            <View style={styles.content}>
                <Text style={styles.contentTitle}>
                    Kasus COVID-19
                </Text>
                    <View style={styles.kasus}>
                        <Text style={styles.kasusTitle}>
                            Positif COVID-19
                        </Text>
                        <Text style={styles.kasusNumber}>
                            {route.params.key.attributes.Kasus_Posi}
                        </Text>
                        <Text style={styles.kasusTitle}>
                            Negatif COVID-19
                        </Text>
                        <Text style={styles.kasusNumber}>
                            {route.params.key.attributes.Kasus_Semb}
                        </Text>
                        <Text style={styles.kasusTitle}>
                            Meninggal
                        </Text>
                        <Text style={styles.kasusNumber}>
                            {route.params.key.attributes.Kasus_Meni}
                        </Text>
                    </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },

    backgroundImage: {
        width: '100%',
        height: 232,
        alignItems:'center',
        justifyContent: 'center'
    },


    titleProvinsi:{
        color: 'white',
        fontSize: 18,
    },

    Provinsi:{
        color: 'white',
        fontSize: 32,
        marginTop: 20,
        fontWeight: 'bold'
    },
    
    content:{
        marginTop: 50,
        marginHorizontal: 30
    },

    contentTitle:{
        fontSize: 20,
        fontWeight: 'bold'
    },

    kasus:{
        marginTop: 10
    },

    kasusTitle:{
        marginVertical: 18,
    },

    kasusNumber:{
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 40,
        marginBottom: 20
    }

})
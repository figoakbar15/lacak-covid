import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground, TouchableOpacity, FlatList, SafeAreaView, ScrollView} from 'react-native';

export default function Home({route, navigation}){

    const [items, setItems]=useState([]);

    const GetData=()=>{
        axios.get('https://api.kawalcorona.com/indonesia/provinsi')
        .then(res=>{
            const data = (res.data)
            setItems(data)
        })
    }
    
    const onSelectItem=(item)=>{
        return(
            navigation.navigate("Detail",{
                screen : 'Detail',
                key: item,
            })
            )
    }

    useEffect(() => {
        GetData()
    }, [])
    
    return(
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.header}>
                <ImageBackground source={require('../assets/home_header.png')} resizeMode="cover" style={styles.backgroundImage}>
                    <View style={styles.containerKasus}>
                        <Text style={styles.titleKasus}>
                            kasus hari ini
                        </Text>
                        <View style={styles.jumlahKasus}>
                            <View>
                                <Image 
                                    style={styles.kasusPositif}
                                    source={require("../assets/positif.png")}
                                />
                                <Text style={styles.angkaKasusPositif}>
                                    514
                                </Text>
                            </View>
                            <View>
                                <Image 
                                    style={styles.kasusNegatif}
                                    source={require("../assets/negatif.png")}
                                />
                                <Text style={styles.angkaKasusNegatif}>
                                        29
                                </Text>
                            </View>
                            <View>
                                <Image 
                                    style={styles.kasusMeninggal}
                                    source={require("../assets/meninggal.png")}
                                />
                                <Text style={styles.angkaKasusMeninggal}>
                                        48
                                </Text>  
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
            <View style={styles.content}>
            <Text style={styles.contentTitle}>
                    Kasus Positif COVID-19
            </Text>
            <SafeAreaView>
                <FlatList
                data={items}
                keyExtractor={(item, index) => `${item.id}-${index}`}
                renderItem={({item}) =>{
                    return (
                    <TouchableOpacity onPress={()=>onSelectItem(item)}>
                        <View style={styles.provinsi}>
                            <View style={styles.containerProvinsi}>
                                <Text style={styles.namaProvinsi}>
                                    {item.attributes.Provinsi}
                                </Text>
                            
                                <View style={styles.jumlahPositif}>
                                    <Text style={styles.angkaPositif}>
                                        {item.attributes.Kasus_Posi}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    )
                }} />
            </SafeAreaView>
            </View>
        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    backgroundImage: {
        width: '100%',
        height: 232,
    },

    containerKasus:{
        marginTop: 170,
        marginHorizontal: 30,
        borderRadius: 20,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },

    titleKasus:{
        textAlign: 'center',
        fontSize: 20,
        marginTop: 20,
        fontWeight: 'bold'
    },

    kasusPositif:{
        height: 32, 
        width: 32,
        marginVertical: 15,
    },

    kasusNegatif:{
        height: 32, 
        width: 32,
        marginVertical: 15,
        marginHorizontal: 61
    },

    kasusMeninggal:{
        height: 32, 
        width: 32,
        marginVertical: 15,
    },

    jumlahKasus:{
        flexDirection: 'row', 
        marginLeft: 60,
        marginVertical: 7,
        marginBottom: 20

    },

    angkaKasusPositif:{
        fontSize: 16,
        fontWeight: 'bold'
    },

    angkaKasusNegatif:{
        marginHorizontal: 70,
        fontSize: 16,
        fontWeight: 'bold'
    },

    angkaKasusMeninggal:{
        fontSize: 16,
        marginLeft: 5,
        fontWeight: 'bold'
    },

    content:{
        marginHorizontal: 30,
        marginTop: 130
    },
    contentTitle:{
        fontSize: 20,
        fontWeight: 'bold'
    },

    provinsi:{
        marginTop: 20
    },

    containerProvinsi:{
        flexDirection: 'row',
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginBottom: 10,
    },

    namaProvinsi:{
        color: "#FF725E",
        marginVertical: 20,
        marginLeft: 10,
        fontWeight: 'bold',
        fontSize: 20,
        paddingRight: 50,
        flex: 1
    },

    jumlahPositif:{
        justifyContent: 'center',
        backgroundColor: '#FF725E',
        marginLeft: 10,
        paddingHorizontal: 20,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        alignItems: 'flex-end'
    },

    angkaPositif:{
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'flex-end'
    }

})